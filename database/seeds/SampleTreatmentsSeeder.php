<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Treatment;

class SampleTreatmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 1; $i <= 10; ++$i) {
            Treatment::create([
                'name' => $faker->sentence(),
                'description' => $faker->paragraph()
            ]);
        }
    }
}
