<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Client;
use App\Treatment;

class SampleClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 1; $i <= 20; ++$i) {
            $client = Client::create([
                'name' => $faker->firstName(),
                'lastName' => $faker->lastName(),
                'birth_date' => $faker->date()
            ]);

            $client->treatments()->attach([rand(1,10)]);
        }
    }
}
