<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Treatment;

class Client extends Model
{
    public function treatments()
    {
        return $this->belongsToMany(Treatment::class);
    }
}
