<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;

class Treatment extends Model
{
    public function clients()
    {
        return $this->belongsToMany(Client::class);
    }
}
